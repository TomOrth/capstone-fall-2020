# capstone-fall-2020
Code related to Tom Orth's Capstone

## Files

`DataCollector.py` - Collects LeapMotion data. Usage: `python DataCollector.py <name> <mode>` where `<name>` is the file name (without extension) and `<mode>` is either one or hand, depending on the sign (requires python 2)

`Plotter.py` - Plots LeapMotion Data. Usage: `python Plotter.py <filename> <column> <run_plotly>` where `<filename>` is the name of the csv file to load, `<column>` is the column name for the y-axis, and `<run_plotly>` is a parameter for whether or not the user wants plotly (must pass `true` if you want plotly) (requires python 3)

`dlib-test` - A test program to explore the capibilities of dlib and opencv (should work with either version of python)

`main/main.py` - Main entry point for the Leap and ASL program (requires python 2)

To use: 
1. `cd main`
2. `python main.py`

For record mode:
1. `cd main`
2. `python main.py r 1` for recording a 1 handed sign
3. `python main.py r 2` for recording a 2 handed sign

## Feature List Process

1. Gather data on the identified feature
2. Check if the feature meets a required threshold
3. Add it to the appropriate array (static or dynamic) depending on the feature type

### Static features

These are features that remain rather much the same over time

* One hand is present
* Two hands are present
* Whether the fingers are extended on the right hand
* Whether the fingers are extended on the left hand
* If the right and/or left palm is facing the leap
* If the right and/or left palm is facing away from the leap
* If the fingers on the right hand are bent
* If the fingers on the left hand are bent
* If there is a noticable angle between the thumb and palm of the right hand
* If there is a noticable angle between the index and middle finger
* If there is a noticable angle between the middle and ring finger

### Dynamic features

These are features that change over time

* There is a change in the x, y, and/or z direction of the palm of either hand
* The palms are close to each other
* For each finger on the right hand, there is a change in the x, y, or z direction of the distal
* For each finger on the left hand, there is a change in the x, y, or z direction of the distal

NOTE: Not all features are currently used due to the current supported signs. They can be added in future additions to the codebase by following the same pattern as the currently used features.

## Extending the existing code

Currently there are two classes, `main/classifier.TwoHandClassifier` and `main/classifier.OneHandClassifier` for two handed and one handed signs respectively

These classes have instance variables that are used for feature processing in the `decision` function for each class

By overriding some methods in subclasses, you can implement your own implementation for sign classification by replacing the instances in  `main/main.py` on roughly lines 113-114
