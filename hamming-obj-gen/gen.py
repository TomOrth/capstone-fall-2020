"""
Program that generates the hamming data structure for the program
Author: Thomas Orth <ortht2@tcnj.edu>
"""
import json

with open("static.json") as f:
    static = json.load(f)

with open("dynamic.json") as f:
    dynamic = json.load(f)

with open("face.json") as f:
    face = json.load(f)

sign_name = ""
variable_name = ""
static_arr = []
dynamic_arr = []
face_arr = []

print("Enter name of the sign: ")
sign_name = str(input())

print("Enter the object name: ")
variable_name = str(input())

print("Static features. Please answer y (yes) or n (no)")
for i in range(len(static)):
    print(static[i]["prompt"])
    static_arr.append(int(str(input()).lower() == "y"))

print("Dynamic features. Please answer y (yes) or n (no)")
for i in range(len(dynamic)):
    print(dynamic[i]["prompt"])
    dynamic_arr.append(int(str(input()).lower() == "y"))

print("Face features. Please answer y (yes) or n (no)")
for i in range(len(face)):
    print(face[i]["prompt"])
    tmp = int(str(input()).lower() == "y")
    if i == 0 and tmp == 1:
        face_arr.append("CHECK_LOWERED")
    if i == 1 and tmp == 1:
        face_arr.append("CHECK_RAISED")
    if i == 2 and tmp == 1:
        face_arr.append("CHECK_SAD")

print("Paste the below code into hamming.py and update the list of signs (remove the quotes from the values for the face_arr output")
print(variable_name + " = " + f"Hamming(\"{sign_name}\", {len(face_arr) > 0}, {len(face_arr) > 1}, {len(face_arr) > 2})")
print(f"{variable_name}.static_arr = {static_arr}")
print(f"{variable_name}.dynamic_arr = {dynamic_arr}")
print(f"{variable_name}.face_arr = {face_arr}")
