"""
Utility methods to be used in the main program code
Author: Thomas Orth <ortht2@tcnj.edu>
"""
def leap_displacement(arr, cat):
    """
    Gets an array of successive differences from leap motion frames to detect displacement (takes absolute value of the diffs)
    :param arr: Array of containing vector values for leap motion frames
    :param cat: Category of the leap motion array to check (i.e. metacarpal x direction for the index finger)
    :returns: An array of sucessive differences
    """
    diff_res = []
    for i in range(len(arr)-1):
         fI = i
         sI = i+1
         diff = abs(abs(arr[fI][cat]) - abs(arr[sI][cat]))
         diff_res.append(diff)
    return diff_res

def leap_displacement_directional(arr, cat):
    """
    Gets an array of successive differenfces from leap motion to detect displacement but considers the direction (does not take absolute vaue)
    :param arr: Array of containing vector values for leap motion frames
    :param cat: Category of the leap motion array to check (i.e. metacarpal x direction for the index finger)
    :returns: An array of successive differences
    """
    diff_res = []
    for i in range(len(arr)-1):
         fI = i
         sI = i+1
         diff = arr[fI][cat] - arr[sI][cat]
         diff_res.append(diff)
    return diff_res

def is_bent(fingers):
     """
     Determines whether or not the fingers of a hand are bent
     :param fingers: An array of finger objects from a Leap Motion Hand Object
     :returns: An array of 0s and 1s that represent whether or not a finger is bent. Ordered by index to pinky
     """
     res = []
     for fing in fingers:
          inter = fing.bone(2).direction
          proximal = fing.bone(1).direction
          res.append(int(inter.angle_to(proximal) > 0.8))
     return res