"""
Classifier code to handle the decision tree process
Author: Thomas Orth <ortht2@tcnj.edu>
"""

import numpy as np
import utils
import signs

class Classifier(object):
    """
    Class that handles classifying of data for LeapMotion
    """

    def process_frame(self, frame, lowered, raised, sad):
        """
        Process an incoming frame of leap motion data
        """
        raise NotImplementedError

    def decision(self):
        """
        Makes some decision for the decision tree
        """
        raise NotImplementedError

class TwoHandClassifier(Classifier):
    """
    Two hand classifier that handles two hand actions in the leap motion
    """
    def __init__(self):
        self.left_hand_data = []
        self.right_hand_data = []
        
        self.left_palm_data = []
        self.right_palm_data = []
        self.read_codes = []
        self.paused = False

    def process_frame(self, frame, raised, lowered, sad, record):
        """
        Process a frame with facial data
        frame: frame from leap motion
        raised: whether or not the eyebrows are raised
        lowered: whether or not the eyebrows are lowered
        sad: whether the user is sad
        """

        # Facial Expressions
        self.lowered = lowered
        self.raised = raised
        self.sad = sad
        self.mid_finger_dip_angle_r = 0
        self.mid_finger_dip_angle_l = 0
        self.record = record
        self.left_hand = frame.hands.leftmost
        self.right_hand = frame.hands.rightmost

        # Hand data
        left_hand = self.process_hand(frame.hands.leftmost, "l")
        right_hand = self.process_hand(frame.hands.rightmost, "r")

        # Palm data
        left_palm = self.process_palm(frame.hands.leftmost)
        right_palm = self.process_palm(frame.hands.rightmost)
        
        # Finger extended
        self.left_finger_extended = self.check_extended(frame.hands.leftmost.fingers)
        self.right_finger_extended = self.check_extended(frame.hands.rightmost.fingers)

        # Bent fingers
        self.left_finger_bent = utils.is_bent(frame.hands.leftmost.fingers)
        self.right_finger_bent = utils.is_bent(frame.hands.rightmost.fingers)

        # Add frame data
        self.left_hand_data.append(left_hand)
        self.right_hand_data.append(right_hand)
        self.left_palm_data.append(left_palm)
        self.right_palm_data.append(right_palm)

        # Now that we have all the info, we can make a decision
        self.decision()

    def check_extended(self, fingers):
        """
        Check if fingers are extended
        fingers: finger list from a hand
        returns: List of booleans for fingers (thumb, index, mid, ring, pinky)
        """
        res = []
        for f in fingers:
            res.append(f.is_extended)
        return res
    
    def process_hand(self, hand, side):
        """
        Process hand from leap
        hand: hand from leap motion
        returns: List of x,y,z direction for the metacarpal, prox, intermediate, and distal for each finger of the hand (thumb, index, mid, ring, pinky)
        """
        finger_ind = hand.fingers[1]
        finger_mid = hand.fingers[2]
        if side == "l":
            self.mid_finger_dip_angle_l = finger_ind.bone(3).next_joint.angle_to(finger_mid.bone(3).next_joint)
        else:
            self.mid_finger_dip_angle_r = finger_ind.bone(3).next_joint.angle_to(finger_mid.bone(3).next_joint)
        return self.process_fingers(hand.fingers)

    def process_fingers(self, fingers):
        """
        Process fingers for locational data
        fingers: finger list from leap motion
        returns: List of x,y,z direction for the metacarpal, prox, intermediate, and distal for each finger (thumb, index, mid, ring, pinky)
        """
        thumb = []
        index = []
        mid = []
        ring = []
        pink = []
        for finger in fingers:

            # The bone types
            TYPE_METACARPAL = 0
            TYPE_PROXIMAL = 1
            TYPE_INTERMEDIATE = 2 
            TYPE_DISTAL = 3

            FING_THUMB = 0
            FING_INDEX = 1
            FING_MID = 2
            FING_RING = 3
            FING_PINK = 4

            # Bone objects
            prox = finger.bone(TYPE_PROXIMAL).next_joint
            interm = finger.bone(TYPE_INTERMEDIATE).next_joint
            distal = finger.bone(TYPE_DISTAL).next_joint

            # We grab a partial list since a thumb has no metacarpal
            partial = [prox.x, prox.y, prox.z, interm.x, interm.y, interm.z, distal.x, distal.y, distal.z]
            if finger.type == FING_THUMB:
                thumb = partial
            else:
                meta = finger.bone(TYPE_METACARPAL).next_joint
                full = [meta.x, meta.y, meta.z] + partial
                if finger.type == FING_INDEX:
                    index = full
                elif finger.type == FING_MID:
                    mid = full
                elif finger.type == FING_RING:
                    ring = full
                elif finger.type == FING_PINK:
                    pink = full
        return thumb+index+mid+ring+pink
    
    def process_palm(self, hand):
        """
        Process palm data
        hand: Hand to process
        returns: List of x,y,z data for the palm normal and palm position of the hand
        """
        palm_normal = hand.palm_normal
        palm_position = hand.palm_position
        return [palm_normal.x, palm_normal.y, palm_normal.z, palm_position.x, palm_position.y, palm_position.z]

    def decision(self):
        """
        Compute a decision from the leap motion
        """
        THUMB_DIST_X = 6
        THUMB_DIST_Y = 7
        THUMB_DIST_Z = 8
        INDEX_DIST_X = 18
        INDEX_DIST_Y = 19
        INDEX_DIST_Z = 20
        MID_DIST_X = 30
        MID_DIST_Y = 31
        MID_DIST_Z = 32
        RING_DIST_X = 42
        RING_DIST_Y = 43
        RING_DIST_Z = 44
        PINK_DIST_X = 54
        PINK_DIST_Y = 55
        PINK_DIST_Z = 56

        PALM_NORM_Y = 1
        PALM_POS_X = 3
        PALM_POS_Y = 4
        PALM_POS_Z = 5
        PALM_NORM_Y = 1
        PALM_NORM_Z = 2

        static_arr = []
        dynam_arr = []

        # This is a two handed classifier
        static_arr.append(1)

        # Determines which right fingers are extended
        for i in range(len(signs.EXTENDED_RANGE_RIGHT)):
            if (self.right_finger_extended[i]):
                static_arr.append(signs.EXTENDED_RANGE_RIGHT[i])
        
        # Determines which left fingers are extended
        for i in range(len(signs.EXTENDED_RANGE_LEFT)):
            if (self.left_finger_extended[i]):
                static_arr.append(signs.EXTENDED_RANGE_LEFT[i])
        
        # If we have at least 50 frames, we can start computing a decision
        if (len(self.right_hand_data) >= 50):
            # Grab last 50 frames
            right_hand_data = self.right_hand_data[-50:]
            left_hand_data = self.left_hand_data[-50:]

            # Grab the last 3 frames to detect a paused hand
            right_hand_data_pause = self.right_hand_data[-3:]
            left_hand_data_pause = self.left_hand_data[-3:]

            # Grab last 50 frames
            right_palm_data = self.right_palm_data[-50:]
            left_palm_data = self.left_palm_data[-50:]

            # These next blocks calculate the average differences for the different components. Ones that contain "data_pause" in its name is meant fo sign pausing/recording
            # Ones that only end in "data" are meant for classifying signs
            right_hand_thumb_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, THUMB_DIST_X))
            right_hand_index_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, INDEX_DIST_X))
            right_hand_mid_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, MID_DIST_X))
            right_hand_ring_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, RING_DIST_X))
            right_hand_pink_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PINK_DIST_X))

            right_hand_thumb_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, THUMB_DIST_Y))
            right_hand_index_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, INDEX_DIST_Y))
            right_hand_mid_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, MID_DIST_Y))
            right_hand_ring_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, RING_DIST_Y))
            right_hand_pink_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PINK_DIST_Y))

            right_hand_thumb_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, THUMB_DIST_Z))
            right_hand_index_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, INDEX_DIST_Z))
            right_hand_mid_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, MID_DIST_Z))
            right_hand_ring_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, RING_DIST_Z))
            right_hand_pink_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PINK_DIST_Z))

            right_hand_palm_pos_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PALM_POS_X))
            right_hand_palm_pos_x_diff_sign = np.mean(utils.leap_displacement(right_hand_data, PALM_POS_X))
            right_hand_palm_pos_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PALM_POS_Y))
            right_hand_palm_pos_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PALM_POS_Z))
            right_hand_palm_pos_z_diff_sign = np.mean(utils.leap_displacement(right_hand_data, PALM_POS_Z))

            left_hand_thumb_x_diff = np.mean(utils.leap_displacement(left_hand_data_pause, THUMB_DIST_X))
            left_hand_index_x_diff = np.mean(utils.leap_displacement(left_hand_data_pause, INDEX_DIST_X))
            left_hand_mid_x_diff = np.mean(utils.leap_displacement(left_hand_data_pause, MID_DIST_X))
            left_hand_ring_x_diff = np.mean(utils.leap_displacement(left_hand_data_pause, RING_DIST_X))
            left_hand_pink_x_diff = np.mean(utils.leap_displacement(left_hand_data_pause, PINK_DIST_X))

            left_hand_thumb_y_diff = np.mean(utils.leap_displacement(left_hand_data_pause, THUMB_DIST_Y))
            left_hand_index_y_diff = np.mean(utils.leap_displacement(left_hand_data_pause, INDEX_DIST_Y))
            left_hand_mid_y_diff = np.mean(utils.leap_displacement(left_hand_data_pause, MID_DIST_Y))
            left_hand_ring_y_diff = np.mean(utils.leap_displacement(left_hand_data_pause, RING_DIST_Y))
            left_hand_pink_y_diff = np.mean(utils.leap_displacement(left_hand_data_pause, PINK_DIST_Y))

            left_hand_thumb_z_diff = np.mean(utils.leap_displacement(left_hand_data_pause, THUMB_DIST_Z))
            left_hand_index_z_diff = np.mean(utils.leap_displacement(left_hand_data_pause, INDEX_DIST_Z))
            left_hand_mid_z_diff = np.mean(utils.leap_displacement(left_hand_data_pause, MID_DIST_Z))
            left_hand_ring_z_diff = np.mean(utils.leap_displacement(left_hand_data_pause, RING_DIST_Z))
            left_hand_pink_z_diff = np.mean(utils.leap_displacement(left_hand_data_pause, PINK_DIST_Z))

            left_hand_palm_pos_x_diff = np.mean(utils.leap_displacement(left_hand_data_pause, PALM_POS_X))
            left_hand_palm_pos_x_diff_sign = np.mean(utils.leap_displacement(left_hand_data, PALM_POS_X))
            left_hand_palm_pos_y_diff = np.mean(utils.leap_displacement(left_hand_data_pause, PALM_POS_Y))
            left_hand_palm_pos_z_diff = np.mean(utils.leap_displacement(left_hand_data_pause, PALM_POS_Z))
            left_hand_palm_pos_z_diff_sign = np.mean(utils.leap_displacement(left_hand_data, PALM_POS_Z))

            # These set of if statements are to check for paused hands. Each one should be less than 0.1 each so the sum of 5 components should be less than 0.5
            if right_hand_thumb_x_diff+right_hand_index_x_diff+right_hand_mid_x_diff+right_hand_ring_x_diff+right_hand_pink_x_diff < 0.75 \
                and right_hand_thumb_y_diff+right_hand_index_y_diff+right_hand_mid_y_diff+right_hand_ring_y_diff+right_hand_pink_y_diff < 0.75 \
                and right_hand_thumb_z_diff+right_hand_index_z_diff+right_hand_mid_z_diff+right_hand_ring_z_diff+right_hand_pink_z_diff < 0.75 \
                and right_hand_palm_pos_x_diff+right_hand_palm_pos_y_diff+right_hand_palm_pos_z_diff < 0.45:

                if left_hand_thumb_x_diff+left_hand_index_x_diff+left_hand_mid_x_diff+left_hand_ring_x_diff+left_hand_pink_x_diff < 0.75 \
                    and left_hand_thumb_y_diff+left_hand_index_y_diff+left_hand_mid_y_diff+left_hand_ring_y_diff+left_hand_pink_y_diff < 0.75 \
                    and left_hand_thumb_z_diff+left_hand_index_z_diff+left_hand_mid_z_diff+left_hand_ring_z_diff+left_hand_pink_z_diff < 0.75 \
                    and left_hand_palm_pos_x_diff+left_hand_palm_pos_y_diff+left_hand_palm_pos_z_diff < 0.45:
                        if self.record:
                            self.paused = True
                            print "PAUSED"

            # We only want to compute a sign if it is not paused
            if not self.paused:
                # Assign the variables to better names (legacy fix)
                mean_right_x = right_hand_palm_pos_x_diff_sign
                mean_left_x = left_hand_palm_pos_x_diff_sign

                mean_right_z = right_hand_palm_pos_z_diff_sign
                mean_left_z = left_hand_palm_pos_z_diff_sign

                #print "mean z"
                #print mean_left_z

                # Grab just the palm normals
                palm_normal_right_y = [right_palm_data[i][PALM_NORM_Y] for i in range(len(right_palm_data))]
                palm_normal_left_y = [left_palm_data[i][PALM_NORM_Y] for i in range(len(left_palm_data))]

                # Take the average of the palm normals
                right_palm_normal_y_mean = np.mean(palm_normal_right_y)
                left_palm_normal_y_mean = np.mean(palm_normal_left_y)

                # Check if palms are facing the leap
                if (right_palm_normal_y_mean < 0):
                    static_arr.append(signs.PALM_R_FACE_LEAP)
                if (left_palm_normal_y_mean < 0):
                    static_arr.append(signs.PALM_L_FACE_LEAP)

                # Check if palms are away from the leap
                if (right_palm_normal_y_mean > 0):
                    static_arr.append(signs.PALM_R_FACE_AWAY)
                if (left_palm_normal_y_mean > 0):
                    static_arr.append(signs.PALM_L_FACE_AWAY)
                # Check for a noticeable x displacement for palm
                if (mean_right_x >= 3):
                    dynam_arr.append(signs.PALM_X_R)
                if (mean_left_x >= 3):
                    dynam_arr.append(signs.PALM_X_L)

                # Check for a noticeable z displacement for palm
                if (mean_right_z >= 2):
                    dynam_arr.append(signs.PALM_Z_R)
                if (mean_left_z >= 2):
                    dynam_arr.append(signs.PALM_Z_L)

                if (self.mid_finger_dip_angle_r >= 0.4):
                    dynam_arr.append(signs.MID_FINGER_DIP_R)
                if (self.mid_finger_dip_angle_l >= 0.4):
                    dynam_arr.append(signs.MID_FINGER_DIP_L)

                read_sign = signs.Sign("", self.lowered, self.raised, self.sad)

                read_sign.static_arr = static_arr
                read_sign.dynamic_arr = dynam_arr

                if self.record:
                    self.read_codes.append(read_sign)
                else:
                    signs.process_signs(read_sign, signs.sign_list)


class OneHandClassifier(object):
    """
    One hand classifier that handles one hand actions in the leap motion
    """
    def __init__(self):
        self.paused = False
        self.right_hand_data = []
        self.read_codes = []
        self.right_palm_data = []
        self.static = False

    def process_frame(self, frame, raised, lowered, sad, record):
        """
        Process a frame with facial data
        frame: frame from leap motion
        raised: whether or not the eyebrows are raised
        lowered: whether or not the eyebrows are lowered
        sad: whether the user is sad
        """
        self.lowered = lowered
        self.raised = raised
        self.sad = sad
        self.record = record
        self.hand = frame.hands.rightmost

        # Hand information
        right_hand = self.process_hand(frame.hands.rightmost)

        # Palm information
        right_palm = self.process_palm(frame.hands.rightmost)
        
        # Right hand finger extension
        self.right_finger_extended = self.check_extended(frame.hands.rightmost.fingers)

        # Append data
        self.right_hand_data.append(right_hand)
        self.right_palm_data.append(right_palm)

        # Array to determine whether or not fingers are extended (goes in finger order, thumb to pinky)
        self.right_hand_bent_fingers = utils.is_bent(self.hand.fingers)
        
        thumb = self.hand.fingers[0]
        index = self.hand.fingers[1]
        mid = self.hand.fingers[2]
        ring = self.hand.fingers[3]

        # Store the angles of the fingers in order to be used later
        self.thumb_index_angle = thumb.bone(1).direction.angle_to(self.hand.palm_position)
        self.index_mid_angle = index.bone(1).direction.angle_to(mid.bone(1).direction)
        self.mid_ring_angle = mid.bone(1).direction.angle_to(ring.bone(1).direction)

        # Now that we have all the info, we can make a decision
        self.decision()

    def check_extended(self, fingers):
        """
        Check if fingers are extended
        fingers: finger list from a hand
        returns: List of booleans for fingers (thumb, index, mid, ring, pinky)
        """
        res = []
        for f in fingers:
            res.append(f.is_extended)
        return res
    
    def process_hand(self, hand):
        """
        Process hand from leap
        hand: hand from leap motion
        returns: List of x,y,z direction for the metacarpal, prox, intermediate, and distal for each finger of the hand (thumb, index, mid, ring, pinky)
        """
        return self.process_fingers(hand.fingers)

    def process_fingers(self, fingers):
        """
        Process fingers for locational data
        fingers: finger list from leap motion
        returns: List of x,y,z direction for the metacarpal, prox, intermediate, and distal for each finger (thumb, index, mid, ring, pinky)
        """
        thumb = []
        index = []
        mid = []
        ring = []
        pink = []
        for finger in fingers:
            # Bone indexes
            TYPE_METACARPAL = 0
            TYPE_PROXIMAL = 1
            TYPE_INTERMEDIATE = 2 
            TYPE_DISTAL = 3

            # Finger indexes
            FING_THUMB = 0
            FING_INDEX = 1
            FING_MID = 2
            FING_RING = 3
            FING_PINK = 4

            # Bone variables
            prox = finger.bone(TYPE_PROXIMAL).next_joint
            interm = finger.bone(TYPE_INTERMEDIATE).next_joint
            distal = finger.bone(TYPE_DISTAL).next_joint

            # Build the list based on finger type
            partial = [prox.x, prox.y, prox.z, interm.x, interm.y, interm.z, distal.x, distal.y, distal.z]
            if finger.type == FING_THUMB:
                thumb = partial
            else:
                meta = finger.bone(TYPE_METACARPAL).next_joint
                full = [meta.x, meta.y, meta.z] + partial
                if finger.type == FING_INDEX:
                    index = full
                elif finger.type == FING_MID:
                    mid = full
                elif finger.type == FING_RING:
                    ring = full
                elif finger.type == FING_PINK:
                    pink = full
        return thumb+index+mid+ring+pink
    
    def process_palm(self, hand):
        """
        Process palm data
        hand: Hand to process
        returns: List of x,y,z data for the palm normal and palm position of the hand
        """
        palm_normal = hand.palm_normal
        palm_position = hand.palm_position
        return [palm_normal.x, palm_normal.y, palm_normal.z, palm_position.x, palm_position.y, palm_position.z]

    def decision(self):
        """
        Compute a decision from data from the leap motion
        """

        # Indexes for the list elements
        THUMB_DIST_X = 6
        THUMB_DIST_Y = 7
        THUMB_DIST_Z = 8
        INDEX_DIST_X = 18
        INDEX_DIST_Y = 19
        INDEX_DIST_Z = 20
        MID_DIST_X = 30
        MID_DIST_Y = 31
        MID_DIST_Z = 32
        RING_DIST_X = 42
        RING_DIST_Y = 43
        RING_DIST_Z = 44
        PINK_DIST_X = 54
        PINK_DIST_Y = 55
        PINK_DIST_Z = 56
        PALM_NORM_Y = 1
        PALM_POS_X = 3
        PALM_POS_Y = 4
        PALM_POS_Z = 5

        # Generate the arrays before evaluation
        static_arr = []
        dynam_arr = []

        # This is a one handed classifier
        static_arr.append(0)

        # Check the state of a finger being extended or not
        for i in range(len(signs.EXTENDED_RANGE_RIGHT)):
            if (self.right_finger_extended[i]):
                static_arr.append(signs.EXTENDED_RANGE_RIGHT[i])

        # Check the state of a finger being bent or not
        bent = utils.is_bent(self.hand.fingers)
        for i in range(len(signs.BENT_RANGE_RIGHT)):
            if (bent[i]):
                static_arr.append(signs.BENT_RANGE_RIGHT[i])

        # Check if them is far from hand
        thumb = self.hand.fingers[0]
        if (thumb.bone(1).direction.angle_to(self.hand.palm_position) <= 1.7):
            static_arr.append(signs.THUMB_ANGLE_PALM)

        index = self.hand.fingers[1]
        mid = self.hand.fingers[2]
        ring = self.hand.fingers[3]
        if (abs(index.bone(1).direction.angle_to(mid.bone(1).direction)) > 0.6):
            static_arr.append(signs.INDEX_ANGLE_MID)

        if (abs(mid.bone(1).direction.angle_to(ring.bone(1).direction)) > 0.33):
            static_arr.append(signs.MID_ANGLE_RING)


        # If we have at least 50 frames, we can start classifying
        if (len(self.right_hand_data) >= 50):
            right_hand_data = self.right_hand_data[-50:]
            right_hand_data_pause = self.right_hand_data[-10:]
            right_hand_x_diff = utils.leap_displacement(right_hand_data, INDEX_DIST_X)

            # Blocks that compute the mean of the leap displacement that end in "data_pause" i.e. "right_hand_data_pause", are meant for pausing and static signs
            # Ones that end in just "data" i.e. "right_hand_data"
            right_hand_thumb_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, THUMB_DIST_X))
            right_hand_index_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, INDEX_DIST_X))
            right_hand_mid_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, MID_DIST_X))
            right_hand_ring_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, RING_DIST_X))
            right_hand_pink_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PINK_DIST_X))

            right_hand_thumb_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, THUMB_DIST_Y))
            right_hand_index_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, INDEX_DIST_Y))
            right_hand_mid_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, MID_DIST_Y))
            right_hand_ring_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, RING_DIST_Y))
            right_hand_pink_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PINK_DIST_Y))

            right_hand_thumb_y_diff_sign = np.mean(utils.leap_displacement(right_hand_data, THUMB_DIST_Y))
            right_hand_index_y_diff_sign = np.mean(utils.leap_displacement(right_hand_data, INDEX_DIST_Y))
            right_hand_mid_y_diff_sign = np.mean(utils.leap_displacement(right_hand_data, MID_DIST_Y))
            right_hand_ring_y_diff_sign = np.mean(utils.leap_displacement(right_hand_data, RING_DIST_Y))
            right_hand_pink_y_diff_sign = np.mean(utils.leap_displacement(right_hand_data, PINK_DIST_Y))

            right_hand_thumb_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, THUMB_DIST_Z))
            right_hand_index_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, INDEX_DIST_Z))
            right_hand_mid_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, MID_DIST_Z))
            right_hand_ring_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, RING_DIST_Z))
            right_hand_pink_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PINK_DIST_Z))

            right_hand_palm_pos_x_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PALM_POS_X))
            right_hand_palm_pos_y_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PALM_POS_Y))
            right_hand_palm_pos_z_diff = np.mean(utils.leap_displacement(right_hand_data_pause, PALM_POS_Z))

            # These set of if statements are to check for paused hands. Each one should be less than 0.1 each so the sum of 5 components should be less than 0.5
            # If we are in record mode, then the system pauses and does not read more signs
            # If it is not in record mode, it will then identify it as a static sign
            if right_hand_thumb_x_diff+right_hand_index_x_diff+right_hand_mid_x_diff+right_hand_ring_x_diff+right_hand_pink_x_diff < 0.75 \
                and right_hand_thumb_y_diff+right_hand_index_y_diff+right_hand_mid_y_diff+right_hand_ring_y_diff+right_hand_pink_y_diff < 0.75 \
                and right_hand_thumb_z_diff+right_hand_index_z_diff+right_hand_mid_z_diff+right_hand_ring_z_diff+right_hand_pink_z_diff < 0.75 \
                and right_hand_palm_pos_x_diff+right_hand_palm_pos_y_diff+right_hand_palm_pos_z_diff < 0.45:
                        if self.record:
                            self.paused = True
                            print "PAUSED"
                        else:
                            self.static = True
            else:
                self.static = False

            if self.static:
                # grab last 10 frames for the palm data for palm normal values
                right_palm_data_pause = self.right_palm_data[-10:]
                palm_normal_y = [right_palm_data_pause[i][PALM_NORM_Y] for i in range(len(right_palm_data_pause))]
                # Average of the palm normal y values
                right_palm_normal_y_mean = np.mean(palm_normal_y)

                # Check if palms are facing the leap
                if (right_palm_normal_y_mean < 0):
                    static_arr.append(signs.PALM_R_FACE_LEAP)

                # Check if palms are away from the leap
                if (right_palm_normal_y_mean > 0):
                    static_arr.append(signs.PALM_R_FACE_AWAY)

                # Deterministic ordering by keeping in sorted order
                static_arr.sort()
                read_sign = signs.Sign("", self.lowered, self.raised, self.sad)

                # Assign feature lists
                read_sign.static_arr = static_arr
                read_sign.dynamic_arr = dynam_arr

                # If we are in record mode,  we 
                if (self.record):
                    self.read_codes.append(read_sign)
                else:
                    signs.process_signs(read_sign, signs.sign_list) 
            
            # If it is not a static sign and the system is not paused, we process the dynamic sign
            elif not self.static and not self.paused:

                mean_right_x = np.mean(right_hand_x_diff)

                right_palm_data = self.right_palm_data[-50:]

                # Palm normal y data
                palm_normal_y = [right_palm_data[i][PALM_NORM_Y] for i in range(len(right_palm_data))]

                # Average of the palm normal y values
                right_palm_normal_y_mean = np.mean(palm_normal_y)

                # Check if palms are facing the leap
                if (right_palm_normal_y_mean < 0):
                    static_arr.append(signs.PALM_R_FACE_LEAP)

                # Check if palms are away from the leap
                if (right_palm_normal_y_mean > 0):
                    static_arr.append(signs.PALM_R_FACE_AWAY)

                if (mean_right_x >= 1):
                    dynam_arr.append(signs.INDEX_DISTAL_R_X)

                # We check the y diff of each finger
                if (right_hand_index_y_diff_sign >= 0.2):
                    dynam_arr.append(signs.INDEX_DISTAL_R_Y)
                if (right_hand_mid_y_diff_sign >= 0.2):
                    dynam_arr.append(signs.MID_DISTAL_R_Y)
                if (right_hand_ring_y_diff_sign >= 0.2):
                    dynam_arr.append(signs.RING_DISTAL_R_Y)
                if (right_hand_pink_y_diff_sign >= 0.2):
                    dynam_arr.append(signs.PINK_DISTAL_R_Y)
                
                # Construct the sign
                read_sign = signs.Sign("", self.lowered, self.raised, self.sad)
                static_arr.sort()
                dynam_arr.sort()
                read_sign.static_arr = static_arr
                read_sign.dynamic_arr = dynam_arr

                # If it is record mode, append the sign to a list
                # Otherwise, we process it to be classifyed
                if (self.record):
                    self.read_codes.append(read_sign)
                else:
                    signs.process_signs(read_sign, signs.sign_list) 
