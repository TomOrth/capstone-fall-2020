"""
Code related to Sign processing
Author: Thomas Orth <ortht2@tcnj.edu>
"""

# Static arr indexes/labels

# Indicates a one handed sign
ONE_HAND = 0
# Indicates a two handed sign
TWO_HAND = 1
# Whether or not a finger on the right hand is extended. Indexes go from 0-4, which correspond to a finger in order (thumb to pinky)
EXTENDED_RANGE_RIGHT = [2,3,4,5,6]
# Whether or not a finger on the left hand is extended. Indexes go from 0-4, which correspond to a finger in order (thumb to pinky)
EXTENDED_RANGE_LEFT = [7,8,9,10,11]
# Right palm is facing the leap
PALM_R_FACE_LEAP = 12
# Left palm is facing the leap
PALM_L_FACE_LEAP = 13
# Right palm is facing away from the leap
PALM_R_FACE_AWAY = 14
# Left palm is facing away from the leap
PALM_L_FACE_AWAY = 15
# Whether or not a finger on the right hand is bent/curled. Indexes go from 0-4, which correspond to a finger in order (thumb to pinky)
BENT_RANGE_RIGHT = [16,17,18,19,20]
# Whether or not a finger on the left hand is bent/curled. Indexes go from 0-4, which correspond to a finger in order (thumb to pinky)
BENT_RANGE_LEFT = [21,22,23,24,25]
# Currently only for right hand (Only needed for one handed signs)
# Whether the thumb forms a relatively large angle with the palm
THUMB_ANGLE_PALM = 26
# Whether there is a noticable angle between the index and middle finger 
INDEX_ANGLE_MID = 27
# Whether there is a noticable angle between the middle and ring finger
MID_ANGLE_RING = 28

# Dynamic arr location indexes

# There is a change in X direction of the right palm
PALM_X_R = 0
# There is a change in the X direction of the left palm
PALM_X_L = 1
# There is a change in the Y direction of the right palm
PALM_Y_R = 2
# There is a change in the Y direction of the left palm
PALM_Y_L = 3
# There is a change in the Z direction of the right palm
PALM_Z_R = 4
# There is a change in the Z direction of the left palm
PALM_Z_L = 5
# Both palms are close
PALM_CLOSE = 6
# The distal bone of the right thumb experiences a change in the X direction
THUMB_DISTAL_R_X = 7
# The distal bone of the right thumb experiences a change in the Y direction
THUMB_DISTAL_R_Y = 8
# The distal bone of the right thumb experiences a change in the Z direction
THUMB_DISTAL_R_Z = 9
# The distal bone of the right index experiences a change in the X direction
INDEX_DISTAL_R_X = 10
# The distal bone of the right index experiences a change in the Y direction
INDEX_DISTAL_R_Y = 11
# The distal bone of the right index experiences a change in the Z direction
INDEX_DISTAL_R_Z = 12
# The distal bone of the right middle finger experiences a change in the X direction
MID_DISTAL_R_X = 13
# The distal bone of the right middle finger experiences a change in the Y direction
MID_DISTAL_R_Y = 14
# The distal bone of the right middle finger experiences a change in the Z direction
MID_DISTAL_R_Z = 15
# The distal bone of the right ring finger experiences a change in the X direction
RING_DISTAL_R_X = 16
# The distal bone of the right ring finger experiences a change in the Y direction
RING_DISTAL_R_Y = 17
# The distal bone of the right ring finger experiences a change in the Z direction
RING_DISTAL_R_Z = 18
# The distal bone of the right pinky experiences a change in the X direction
PINK_DISTAL_R_X = 19
# The distal bone of the right pinky experiences a change in the Y direction
PINK_DISTAL_R_Y = 20
# The distal bone of the right pinky experiences a change in the Z direction
PINK_DISTAL_R_Z = 21
# The distal bone of the left thumb experiences a change in the X direction
THUMB_DISTAL_L_X = 22
# The distal bone of the left thumb experiences a change in the Y direction
THUMB_DISTAL_L_Y = 23
# The distal bone of the left thumb experiences a change in the Z direction
THUMB_DISTAL_L_Z = 24
# The distal bone of the left index experiences a change in the X direction
INDEX_DISTAL_L_X = 25
# The distal bone of the left index experiences a change in the Y direction
INDEX_DISTAL_L_Y = 26
# The distal bone of the left index experiences a change in the Z direction
INDEX_DISTAL_L_Z = 27
# The distal bone of the left middle finger experiences a change in the X direction
MID_DISTAL_L_X = 28
# The distal bone of the left middle finger experiences a change in the Y direction
MID_DISTAL_L_Y = 29
# The distal bone of the left middle finger experiences a change in the Z direction
MID_DISTAL_L_Z = 30
# The distal bone of the left ring finger experiences a change in the X direction
RING_DISTAL_L_X = 31
# The distal bone of the left ring finger experiences a change in the Y direction
RING_DISTAL_L_Y = 32
# The distal bone of the left ring finger experiences a change in the Z direction
RING_DISTAL_L_Z = 33
# The distal bone of the left pinky experiences a change in the X direction
PINK_DISTAL_L_X = 34
# The distal bone of the left pinky experiences a change in the Y direction
PINK_DISTAL_L_Y = 35
# The distal bone of the left pinky experiences a change in the Z direction
PINK_DISTAL_L_Z = 36
# The right middle finger dips in (used for the sign for excited)
MID_FINGER_DIP_R = 37
# The left middle finger dips in (used for the sign for excited)
MID_FINGER_DIP_L = 38

# Facial data check values

# Passed if the sign should check the status of lowered eyebrows
CHECK_LOWERED = 0
# Passed if the sign should check the status of raised eyebrows
CHECK_RAISED = 1
# Passed if the sign should check the status of a person making a sad face 
CHECK_SAD = 2

class Sign(object):
    """
    Class to encapsulate the features of a sign
    """
    def __init__(self, sign, lowered, raised, sad):
        # Static information over time
        self.static_arr = []
        # Dynamic information over time
        self.dynamic_arr = []
        # Facial data
        self.face_arr = []
        self.lowered = lowered
        self.raised = raised
        self.sad = sad
        self.sign = sign
    
    def __str__(self):
        return self.sign

def process_signs(read_sign, signs):
    found_sign = False
    """
    Process a list of signs to compare to the built object to determine what sign was built
    :param read_sign: The sign read by the leap motion
    :param signs: A list of defined signs
    """
    for sign in signs:
        #print str(read_sign.static_arr)  + " == " + str(sign.static_arr)
        #print str(read_sign.dynamic_arr) + " == " + str(sign.dynamic_arr) + "\n"
        if read_sign.static_arr == sign.static_arr and read_sign.dynamic_arr == sign.dynamic_arr:
            face_check = True
            for check in sign.face_arr:
                if check == CHECK_LOWERED:
                    face_check = (read_sign.lowered == sign.lowered)
                if check == CHECK_RAISED:
                    face_check = (read_sign.raised == sign.raised)
                if check == CHECK_SAD:
                    face_check = (read_sign.sad == sign.sad)
            if face_check:
                found_sign = True
                print "You signed: " + str(sign)
                break
    if not found_sign:
        print "I don't know"

# Static feature amount (odd numbered since the angle of the fingers is only done for one hand right now)
STATIC_SIZE = 29
# Dynamic feature amount (odd numbered since there is a feature for if the palms are close)
DYNAMIC_SIZE = 39

# List of signs
# Order for the values in the arrays should be in ascending order

# List of Dynamic signs (Signs that move)

# Sign for what, no eyebrows lowered
what = Sign("what", False, False, False)
what.static_arr = [TWO_HAND,EXTENDED_RANGE_RIGHT[0],EXTENDED_RANGE_RIGHT[1],EXTENDED_RANGE_RIGHT[2],EXTENDED_RANGE_RIGHT[3],EXTENDED_RANGE_RIGHT[4],EXTENDED_RANGE_LEFT[0],EXTENDED_RANGE_LEFT[1],EXTENDED_RANGE_LEFT[2],EXTENDED_RANGE_LEFT[3],EXTENDED_RANGE_LEFT[4],PALM_R_FACE_AWAY,PALM_L_FACE_AWAY]
what.dynamic_arr = [PALM_X_R,PALM_X_L]
what.face_arr = [CHECK_LOWERED]

# Sign for what with eyebrows lowered
what_q = Sign("what?", True, False, False)
what_q.static_arr = [TWO_HAND,EXTENDED_RANGE_RIGHT[0],EXTENDED_RANGE_RIGHT[1],EXTENDED_RANGE_RIGHT[2],EXTENDED_RANGE_RIGHT[3],EXTENDED_RANGE_RIGHT[4],EXTENDED_RANGE_LEFT[0],EXTENDED_RANGE_LEFT[1],EXTENDED_RANGE_LEFT[2],EXTENDED_RANGE_LEFT[3],EXTENDED_RANGE_LEFT[4],PALM_R_FACE_AWAY,PALM_L_FACE_AWAY]
what_q.dynamic_arr = [PALM_X_R,PALM_X_L]
what_q.face_arr = [CHECK_LOWERED]

# Sign for sad
sad = Sign("sad", False, False, True)
sad.static_arr = [TWO_HAND,EXTENDED_RANGE_RIGHT[0],EXTENDED_RANGE_RIGHT[1],EXTENDED_RANGE_RIGHT[2],EXTENDED_RANGE_RIGHT[3],EXTENDED_RANGE_RIGHT[4],EXTENDED_RANGE_LEFT[0],EXTENDED_RANGE_LEFT[1],EXTENDED_RANGE_LEFT[2],EXTENDED_RANGE_LEFT[3],EXTENDED_RANGE_LEFT[4],PALM_R_FACE_AWAY,PALM_L_FACE_AWAY]
sad.dynamic_arr = [PALM_Z_R,PALM_Z_L]
sad.face_arr = [CHECK_SAD]

# Sign for where w/o eyebrows lowered
where = Sign("where", False, False, False)
where.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[1],PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[2],BENT_RANGE_RIGHT[3],BENT_RANGE_RIGHT[4],THUMB_ANGLE_PALM, INDEX_ANGLE_MID]
where.dynamic_arr = [INDEX_DISTAL_R_X,INDEX_DISTAL_R_Y,MID_DISTAL_R_Y,RING_DISTAL_R_Y,PINK_DISTAL_R_Y]
where.face_arr = [CHECK_LOWERED]

# Sign for where w/ eyebrows lowered
where_q = Sign("where?", True, False, False)
where_q.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[1],PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[2],BENT_RANGE_RIGHT[3],BENT_RANGE_RIGHT[4],THUMB_ANGLE_PALM, INDEX_ANGLE_MID]
where_q.dynamic_arr = [INDEX_DISTAL_R_X,INDEX_DISTAL_R_Y,MID_DISTAL_R_Y,RING_DISTAL_R_Y,PINK_DISTAL_R_Y]
where_q.face_arr = [CHECK_LOWERED]

# Sign for who w/o eyebrows lowered
who = Sign("who", False, False, False)
who.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[0],EXTENDED_RANGE_RIGHT[1],PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[2],BENT_RANGE_RIGHT[3],BENT_RANGE_RIGHT[4]]
who.dynamic_arr = [INDEX_DISTAL_R_X,INDEX_DISTAL_R_Y,MID_DISTAL_R_Y,RING_DISTAL_R_Y,PINK_DISTAL_R_Y]
who.face_arr = [CHECK_LOWERED]

# Sign for who w/ eyebrows lowered
who_q = Sign("who?", True, False, False)
who_q.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[0],EXTENDED_RANGE_RIGHT[1],PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[2],BENT_RANGE_RIGHT[3],BENT_RANGE_RIGHT[4]]
who_q.dynamic_arr = [INDEX_DISTAL_R_X,INDEX_DISTAL_R_Y,MID_DISTAL_R_Y,RING_DISTAL_R_Y,PINK_DISTAL_R_Y]
who_q.face_arr = [CHECK_LOWERED]

# Sign for mad
mad = Sign("mad", True, False, False)
mad.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[1],EXTENDED_RANGE_RIGHT[2],EXTENDED_RANGE_RIGHT[3],EXTENDED_RANGE_RIGHT[4],PALM_R_FACE_AWAY]
mad.dynamic_arr = [INDEX_DISTAL_R_Y,MID_DISTAL_R_Y,RING_DISTAL_R_Y,PINK_DISTAL_R_Y]
mad.face_arr = [CHECK_LOWERED]

# This sign is kinda sporadic in terms of stable detection so commented out for the time being
"""
excited = Sign("excited", False, False, False)
excited.static_arr = [0,1,1,1,0,1,1,1,1,0,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0]
excited.dynamic_arr = [0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1]
"""

# Static signs

# The sign for the letter A
a = Sign("A", False, False, False)
a.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[0],PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[1],BENT_RANGE_RIGHT[2],BENT_RANGE_RIGHT[3],BENT_RANGE_RIGHT[4]]
a.dynamic_arr = []

# The sign for the letter B
b = Sign("B", False, False, False)
b.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[1],EXTENDED_RANGE_RIGHT[2],EXTENDED_RANGE_RIGHT[3],EXTENDED_RANGE_RIGHT[4],PALM_R_FACE_LEAP,THUMB_ANGLE_PALM]
b.dynamic_arr = []

# The sign for the letter C
c = Sign("C", False, False, False)
c.static_arr = [ONE_HAND,PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[1],BENT_RANGE_RIGHT[2],BENT_RANGE_RIGHT[3],BENT_RANGE_RIGHT[4],THUMB_ANGLE_PALM]
c.dynamic_arr = []

# The sign for the letter D
d = Sign("D", False, False, False)
d.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[1],PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[2],BENT_RANGE_RIGHT[3],BENT_RANGE_RIGHT[4],THUMB_ANGLE_PALM,INDEX_ANGLE_MID]
d.dynamic_arr = []

# The sign for the letter F
f = Sign("F", False, False, False)
f.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[2],EXTENDED_RANGE_RIGHT[3],EXTENDED_RANGE_RIGHT[4],PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[1],THUMB_ANGLE_PALM,INDEX_ANGLE_MID]
f.dynamic_arr = []

# The sign for the letter I
i = Sign("I", False, False, False)
i.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[0],EXTENDED_RANGE_RIGHT[4],PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[1],BENT_RANGE_RIGHT[2],BENT_RANGE_RIGHT[3]]
i.dynamic_arr = []

# The sign for the letter U
u = Sign("U", False, False, False)
u.static_arr = [ONE_HAND, EXTENDED_RANGE_RIGHT[1], EXTENDED_RANGE_RIGHT[2], PALM_R_FACE_LEAP, BENT_RANGE_RIGHT[3], BENT_RANGE_RIGHT[4], THUMB_ANGLE_PALM, MID_ANGLE_RING]
u.dynamic_arr = []

# The sign for the letter V
v = Sign("V", False, False, False)
v.static_arr = [ONE_HAND, EXTENDED_RANGE_RIGHT[1], EXTENDED_RANGE_RIGHT[2], PALM_R_FACE_LEAP, BENT_RANGE_RIGHT[3], BENT_RANGE_RIGHT[4], THUMB_ANGLE_PALM, INDEX_ANGLE_MID, MID_ANGLE_RING]
v.dynamic_arr = []

# The sign for W
# The sign for the letter V
w = Sign("W", False, False, False)
w.static_arr = [ONE_HAND, EXTENDED_RANGE_RIGHT[1], EXTENDED_RANGE_RIGHT[2], EXTENDED_RANGE_RIGHT[3], PALM_R_FACE_LEAP, BENT_RANGE_RIGHT[4], THUMB_ANGLE_PALM, INDEX_ANGLE_MID, MID_ANGLE_RING]
w.dynamic_arr = []

# The sign for the letter Y
y = Sign("Y", False, False, False)
y.static_arr = [ONE_HAND,EXTENDED_RANGE_RIGHT[0],EXTENDED_RANGE_RIGHT[4],PALM_R_FACE_LEAP,BENT_RANGE_RIGHT[1],BENT_RANGE_RIGHT[2],BENT_RANGE_RIGHT[3], THUMB_ANGLE_PALM]
y.dynamic_arr = []

# Supported signs
sign_list = [what, what_q, sad, where, where_q, who, who_q, mad, a, b, c, d, f, i, u, v, w, y]
