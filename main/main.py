"""
Main program for ASL Sign Detection
Author: Thomas Orth <ortht2@tcnj.edu>
"""
import os, sys, inspect, thread, time
import cv2, dlib
import classifier as clfr
import signs
import pickle
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
# Windows and Linux
#arch_dir = '../lib/x64' if sys.maxsize > 2**32 else '../lib/x86'
# Mac
arch_dir = os.path.abspath(os.path.join(src_dir, os.environ["LEAP_PYTHON"]))

sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))

# This value changes occassionally...its either 0 or 1, depending on how the leap motion registers with the laptop
cap = cv2.VideoCapture(0)

detector = dlib.get_frontal_face_detector()
# Load the predictor
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
# The booleans are used for the state of the vision code
# The array is for the differnce in the eyebrows distance
initial = True
raised = True
lowered = True
sad = True
y_brow = []

import Leap


def camera():
    """
    Main entry point for OpenCV processing
    """

    # Global variables needed due to the setup of the LeapMotion Listener
    global initial
    global raised
    global lowered
    global sad
    initial = True
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()
        raised = True
        lowered = True
        sad = True
        # Our operations on the frame come here
        gray = cv2.cvtColor(src=frame, code=cv2.COLOR_BGR2GRAY)
        faces = detector(gray)
        for face in faces:
            x1 = face.left() # left point
            y1 = face.top() # top point
            x2 = face.right() # right point
            y2 = face.bottom() # bottom point
            # Create landmark object
            landmarks = predictor(image=gray, box=face)
            # Grab the initial location of the landmarks
            if initial:
                # 19-25 are the eyebrows
                # 48 and 54 is the points on the side of the face
                y_brow = [int(landmarks.part(n).y) for n in range(19,25)]
                y_lip = [int(landmarks.part(n).y) for n in [48,54]]
                initial = False
            arr = [int(landmarks.part(n).y) for n in range(19,25) ]
            sad_arr = [int(landmarks.part(n).y) for n in [48,54]]
            diff = [y_brow[i] - arr[i] for i in range(len(arr))]
            sad_diff = [y_lip[i] - sad_arr[i] for i in range(len(sad_arr))]
            
            # Compare the eyebrow displacement to deterimne the state of the eyebrows
            for idx in range(len(diff)-3):
                if diff[idx] < 0:
                    raised = False
                    break

            # Compare the eyebrow displacement to deterimne the state of the eyebrows
            for idx in range(len(diff)):
                if diff[idx] > 0:
                    lowered = False
                    break

            # Check for a sad face
            if sad_diff[0] > -10:
                sad = False

            if raised:
                print "Eyebrows are raised"
            if lowered:
                print "Eyebrows are lowered"
            if sad:
                print "You are sad"

            # Loop through all the points
            for n in [48,54]:
                x = landmarks.part(n).x
                y = landmarks.part(n).y
                # Draw a circle
                cv2.circle(img=frame, center=(x, y), radius=3, color=(0, 255, 0), thickness=-1)
        frame = cv2.resize(frame, (300,300))
        cv2.imshow("frame", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    # When everything done, release the capture
    cap.release()
    # Close all windows
    cv2.destroyAllWindows()

# Create our classifier objects
two_hand_classifier = clfr.TwoHandClassifier()
one_hand_classifier = clfr.OneHandClassifier()

class SampleListener(Leap.Listener):
    """
    Listener used for Leap Motion connection
    """

    def on_connect(self, controller):
        print "Connected"

    def on_frame(self, controller):
        """
        Process frames from the leap motion
        """
        frame = controller.frame()
        global two_hand_classifier
        global raised
        global lowered
        global sad
        # if it is a two handed sign and if it is not paused
        if (len(frame.hands) == 2):
            if (not two_hand_classifier.paused):
                two_hand_classifier.process_frame(frame, raised, lowered, sad, len(sys.argv) > 1 and sys.argv[1] == "r")
        elif (len(frame.hands) == 1):
            if (not one_hand_classifier.paused):
                one_hand_classifier.process_frame(frame, raised, lowered, sad, len(sys.argv) > 1 and sys.argv[1] == "r")
        
def main():
    # Create a sample listener and controller
    listener = SampleListener()
    controller = Leap.Controller()
    print("main")

    # Setup variables
    initial = True
    raised = True
    lowered = True
    sad = True

    # Load custom signs if the pickle file exists
    custom_sign_list = []
    if os.path.isfile(".signs.pickle"):
        with open(".signs.pickle", "rb") as f:
            custom_sign_list = pickle.load(f)
    signs.sign_list += custom_sign_list
    # Have the sample listener receive events from the controller
    controller.add_listener(listener)

    # Keep this process running until Ctrl+c is pressed
    print "Press Ctrl+c to quit..."
    try:
        camera()
    except KeyboardInterrupt:
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)
    
    
    # If we are in record mode, we grab the sign that was processed
    read_sign = None
    if (len(sys.argv) > 2 and sys.argv[2] == "2" and len(two_hand_classifier.read_codes) >= 20):
        read_sign = two_hand_classifier.read_codes[-10]

    elif (len(sys.argv) > 2 and sys.argv[2] == "1" and len(one_hand_classifier.read_codes) >= 20):
        read_sign = one_hand_classifier.read_codes[-10]

    # If we have a valid sign, we
    if (read_sign != None):
        print "Static features: " + str(read_sign.static_arr)
        print "Dynamic features: " + str(read_sign.dynamic_arr)
        read_sign.sign = str(raw_input("Enter sign name: "))

        # Set the face check information
        if (str(raw_input("Check status of lowered eyebrows? ")) == "y"):
            read_sign.face_arr.append(0)

        if (str(raw_input("Check status of raised eyebrows? ")) == "y"):
            read_sign.face_arr.append(1)
            
        if (str(raw_input("Check status of sad face? ")) == "y"):
            read_sign.face_arr.append(2)
        
        # Store the sign
        custom_sign_list.append(read_sign)
        with open(".signs.pickle", "wb") as f:
            pickle.dump(custom_sign_list, f)

main()
