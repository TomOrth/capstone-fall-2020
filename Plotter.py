"""
Simple plotter program for Leap Motion Data
Author: Thomas Orth <ortht2@tcnj.edu>
"""

import matplotlib.pyplot as plt
import pandas as pd
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import sys

class Plotter(object):
    """
    Plotter superclass
    A Plotter is meant to plot some data, agnostic of the library
    """
    def __init__(self, df):
        """
        Initilaize the Plotter with a relevant dataframe
        :param df: A pandas dataframe for the plot
        """
        self.df = df

    def plot_data(self, column, output=""):
        """
        Plots the data from the dataframe and writes it to a file. Assumes there is a dataframe column called Time
        :param column: The column for the dataframe to plot for the y-axis
        :param output: Optional parameter for output of plot to file
        """
        raise NotImplementedError

class MPLPlotter(Plotter):
    """
    A Plotter class specifically for Matplotlib
    """
    def __init__(self, df):
        super().__init__(df)

    def plot_data(self, column, output=""):
        plt.xlabel("Time")
        plt.ylabel(column)
        plt.title("Data for" + column + " over time")
        plt.plot(self.df["Time"], self.df[column])
        plt.savefig(output)

class PlotlyPlotter(Plotter):
    """
    A Plotter class specifically for Plotly
    """
    def __init__(self, df):
        super().__init__(df)

    def plot_data(self, column, output=""):
        fig = make_subplots(
        rows=2, cols=1,
        shared_xaxes=True,
        vertical_spacing=0.03,
        specs=[[{"type": "table"}],
           [{"type": "scatter"}]]
        )
        fig.add_trace(
            go.Scatter(
                x=df["Time"],
                y=df[column],
                mode="lines",
                name=column + " over time"
            ),
            row=2, col=1
        )

        fig.add_trace(
            go.Table(
                header=dict(
                    values=["Time", column],
                    font=dict(size=10),
                    align="left"
                ),
                cells=dict(
                    values=[df[k].tolist() for k in ["Time", column]],
                    align = "left")
            ),
            row=1, col=1
        )
        fig.update_layout(
            height=800,
            showlegend=False,
            title_text=column + " over time",
        )

        fig.show()


filename = sys.argv[1]
column = " " + sys.argv[2]
run_plotly = sys.argv[3] if len(sys.argv) > 3 else "true"
df = pd.read_csv(filename)


mpl_plot = MPLPlotter(df)
plotly_plot = PlotlyPlotter(df)

mpl_plot.plot_data(column, filename.split("_")[0] + "_" + column.strip() + ".jpg")
if run_plotly == "true":
    plotly_plot.plot_data(column)
