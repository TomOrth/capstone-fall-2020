import cv2
import numpy as np
import dlib
import time
# Load the detector
cap = cv2.VideoCapture(0)
detector = dlib.get_frontal_face_detector()
# Load the predictor
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
# read the image
initial = True
raised = True
lowered = True
sad = True
y_brow = []
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    raised = True
    lowered = True
    sad = True
    # Our operations on the frame come here
    gray = cv2.cvtColor(src=frame, code=cv2.COLOR_BGR2GRAY)
    faces = detector(gray)
    for face in faces:
        x1 = face.left() # left point
        y1 = face.top() # top point
        x2 = face.right() # right point
        y2 = face.bottom() # bottom point
        # Create landmark object
        landmarks = predictor(image=gray, box=face)
        if initial:
            y_brow = [int(landmarks.part(n).y) for n in range(19,25)]
            y_lip = [int(landmarks.part(n).y) for n in [48,54]]
            print "Initial:"
            print y_lip
            initial = False
        arr = [int(landmarks.part(n).y) for n in range(19,25) ]
        sad_arr = [int(landmarks.part(n).y) for n in [48,54]]
        sad_diff = [y_lip[i] - sad_arr[i] for i in range(len(sad_arr))]
        diff = [y_brow[i] - arr[i] for i in range(len(arr))]
        print sad_diff
        for idx in range(len(diff)):
            if diff[idx] < 2:
                raised = False
                break
        for idx in range(len(diff)):
            if diff[idx] > -2:
                print idx
                print diff[idx]
                lowered = False
                break
        if raised:
            print "Eyebrows raised"
        if lowered:
            print "Eyebrows lowered"
	if abs(sad_diff[0]) >= 7:
            print "Sad face"
        # Loop through all the points
        for n in range(19, 25):
            x = landmarks.part(n).x
            y = landmarks.part(n).y
            # Draw a circle
            cv2.circle(img=frame, center=(x, y), radius=3, color=(0, 255, 0), thickness=-1)

        for n in [48,54]:
            x = landmarks.part(n).x
            y = landmarks.part(n).y
            # Draw a circle
            cv2.circle(img=frame, center=(x, y), radius=3, color=(0, 255, 0), thickness=-1)
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
# Close all windows
cv2.destroyAllWindows()
