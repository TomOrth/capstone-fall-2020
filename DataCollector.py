"""
Simple data collection program for Leap Motion Data
Author: Thomas Orth <ortht2@tcnj.edu>
"""

# Block of code that handles setting up the leap motion library
# Path is stored in LEAP_PYTHON
import os, sys, inspect, thread, time
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
# Mac
arch_dir = os.path.abspath(os.path.join(src_dir, os.environ["LEAP_PYTHON"]))

sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))

# Leap Motion package
import Leap
# Datetime for time tracking
from datetime import datetime
import csv
#misc
import math

# Columns for the CSV
firstrow =  ['', 'Time', 
                  
                  ' thumbProximal_L_X', ' thumbProximal_L_Y', ' thumbProximal_L_Z', 
                  ' thumbInterm_L_X', ' thumbInterm_L_Y', ' thumbInterm_L_Z', 
                  ' thumbDistal_L_X', ' thumbDistal_L_Y', ' thumbDistal_L_Z', 
                  
                  ' indexMeta_L_X', ' indexMeta_L_Y', ' indexMeta_L_Z', 
                  ' indexProximal_L_X', ' indexProximal_L_Y', ' indexProximal_L_Z', 
                  ' indexInterm_L_X', ' indexInterm_L_Y', ' indexInterm_L_Z', 
                  ' indexDistal_L_X', ' indexDistal_L_Y', ' indexDistal_L_Z',
                  
                  ' middleMeta_L_X', ' middleMeta_L_Y', ' middleMeta_L_Z', 
                  ' middleProximal_L_X', ' middleProximal_L_Y', ' middleProximal_L_Z', 
                  ' middleInterm_L_X', ' middleInterm_L_Y', ' middleInterm_L_Z', 
                  ' middleDistal_L_X', ' middleDistal_L_Y', ' middleDistal_L_Z',
                  
                  ' ringMeta_L_X', ' ringMeta_L_Y', ' ringMeta_L_Z', 
                  ' ringProximal_L_X', ' ringProximal_L_Y', ' ringProximal_L_Z', 
                  ' ringInterm_L_X', ' ringInterm_L_Y', ' ringInterm_L_Z', 
                  ' ringDistal_L_X', ' ringDistal_L_Y', ' ringDistal_L_Z',
                  
                  ' pinkMeta_L_X', ' pinkMeta_L_Y', ' pinkMeta_L_Z', 
                  ' pinkProximal_L_X', ' pinkProximal_L_Y', ' pinkProximal_L_Z', 
                  ' pinkInterm_L_X', ' pinkInterm_L_Y', ' pinkInterm_L_Z', 
                  ' pinkDistal_L_X', ' pinkDistal_L_Y', ' pinkDistal_L_Z',
                  ' palmNormal_L_X', ' palmNormal_L_Y', ' palmNormal_L_Z',
                  ' palmPosition_L_X', ' palmPosition_L_Y', ' palmPosition_L_Z']

class Collector(object):

    """
    A superclass for collecting leap motion data
    """

    def __init__(self, filename):
        """
        Creates a CSV writer
        filename: the name of the csv file
        """
        file = open(filename, "wb")
        file.truncate()
        self.writer = csv.writer(file)
        self.writer.writerow(firstrow)
    
    def process_frame(self, frame):
        """
        Processes a frame of data
        frame: Frame from the Leap Motion
        """
        raise NotImplementedError

class OneHandCollector(Collector):
    """
    Collector that collects data from one hand
    """
    
    def __init__(self, filename, side):
        super(OneHandCollector, self).__init__(filename)
        self.frame_count = 0
        self.row = []
        self.thumb = []
        self.index = []
        self.mid = []
        self.ring = []
        self.pink = []
        self.palm = []
        self.side = side # 0 for right, 1 for left

    def process_palm(self, hand):
        palm_normal = hand.palm_normal
        palm_position = hand.palm_position
        self.palm = [palm_normal.x, palm_normal.y, palm_normal.z, palm_position.x, palm_position.y, palm_position.z]

    def process_finger(self, finger):
        TYPE_METACARPAL = 0
        TYPE_PROXIMAL = 1
        TYPE_INTERMEDIATE = 2 
        TYPE_DISTAL = 3
        prox = finger.bone(TYPE_PROXIMAL).next_joint
        interm = finger.bone(TYPE_INTERMEDIATE).next_joint
        distal = finger.bone(TYPE_DISTAL).next_joint

        partial = [prox.x, prox.y, prox.z, interm.x, interm.y, interm.z, distal.x, distal.y, distal.z]
        if finger.type == 0:
            self.thumb = partial
        else:
            meta = finger.bone(TYPE_METACARPAL).next_joint
            full = [meta.x, meta.y, meta.z] + partial
            if finger.type == 1:
                self.index = full
            elif finger.type == 2:
                self.mid = full
            elif finger.type == 3:
                self.ring = full
            elif finger.type == 4:
                self.pink = full

    def process_fingers(self, fingers):
        for finger in fingers:
            self.process_finger(finger)

    def process_frame(self, frame):
        if self.side == 0:
            hand = frame.hands.rightmost
        else:
            hand = frame.hands.leftmost
        self.process_palm(hand)
        self.process_fingers(hand.fingers)
        row = [self.frame_count, datetime.now().time()] + self.thumb + self.index + self.mid + self.ring + self.pink + self.palm
        print row
        self.writer.writerow(row)
        self.frame_count += 1
        self.thumb = []
        self.index = []
        self.mid = []
        self.ring = []
        self.pink = []
        self.palm = []


right_collector = OneHandCollector(sys.argv[1] + "_right.csv", 0)
left_collector = OneHandCollector(sys.argv[1] + "_left.csv", 1)

class SampleListener(Leap.Listener):
    global right_collector
    global left_collector

    #if connected prints out collected
    def on_connect(self, controller):
        print ("Connected")

    #when it is collecting frames
    def on_frame(self, controller):
        frame = controller.frame()
        
        #when there is a hand in frame calls hand function
        if (not frame.hands.is_empty):
            
            if (sys.argv[2] == "one" and len(frame.hands) < 2):
                right_collector.process_frame(frame)
            elif(sys.argv[2] == "two" and len(frame.hands) == 2):
                right_collector.process_frame(frame)
                left_collector.process_frame(frame)


if __name__ == "__main__":

    raw_input("Will be collecting data, press Enter to start")

    #creates listener
    listener = SampleListener()
    
    #creates controller
    controller = Leap.Controller()
    
    # Have the sample listener receive events from the controller
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print ("Press Enter to quit...")
    
    try:
        #reads input
        sys.stdin.readline()
        
    except KeyboardInterrupt:
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)
